package ru.kazakov.tm.controller;

import java.util.Scanner;

public class AbstractController {

    protected final  Scanner scanner = new Scanner(System.in);

    protected boolean checkAuth(Long session){
        boolean checkAuth = session == null;
        if(checkAuth){
            System.out.println("Access denied!");
        }
        return checkAuth;
    }

}
